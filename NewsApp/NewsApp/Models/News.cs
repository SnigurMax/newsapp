﻿using System;

namespace NewsApp.Models
{
    public class News
    {
        public DateTime Time { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
