﻿using Acr.UserDialogs;
using AsyncAwaitBestPractices;
using AsyncAwaitBestPractices.MVVM;
using Autofac;
using NewsApp.Interfaces;
using NewsApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;
using XamarinFormsMvvmAdaptor;

namespace NewsApp.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IApiService apiService;

        private List<News> _newsList;
        public List<News> NewsList
        {
            get => _newsList;
            set => SetProperty(ref _newsList, value);
        }

        public IAsyncCommand<News> OpenDetailsCommand { get; }

        public MainViewModel(IApiService apiService)
        {
            this.apiService = apiService;
            OpenDetailsCommand = new AsyncCommand<News>(news => OpenNewsDetailsAsync(news));
            _newsList = new List<News>();
        }

        private async Task OpenNewsDetailsAsync(News news)
        {
            await App.DiContainer.Resolve<INavController>().DiPushAsync(App.DiContainer.Resolve<DetailsViewModel>(), news);
        }

        public override Task InitializeAsync(object navigationData)
        {
            LoadDataAsync().SafeFireAndForget();
            return base.InitializeAsync(navigationData);
        }

        private async Task LoadDataAsync()
        {
            try
            {
                var news = await apiService.GetNewsAsync();

                if (news != null)
                    NewsList = new List<News>(news);
                else
                    UserDialogs.Instance.Alert("Check your internet connection", "Hmmm...");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
