﻿using AsyncAwaitBestPractices.MVVM;
using NewsApp.Models;
using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace NewsApp.ViewModels
{
    public class DetailsViewModel : BaseViewModel
    {
        private News _news;
        public News News
        {
            get => _news;
            set => SetProperty(ref _news, value);
        }

        public IAsyncCommand OpenLinkCommand { get; }

        public DetailsViewModel() 
        {
            OpenLinkCommand = new AsyncCommand(OpenBrowserAsync);
        }

        public override Task InitializeAsync(object navigationData)
        {
            News = navigationData as News;
            return base.InitializeAsync(navigationData);
        }

        public async Task OpenBrowserAsync()
        {
            try
            {
                await Browser.OpenAsync(News.Url, BrowserLaunchMode.SystemPreferred);
            }
            catch (Exception ex)
            {
                // An unexpected error occured. No browser may be installed on the device.
            }
        }
    }
}
