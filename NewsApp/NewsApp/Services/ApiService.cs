﻿using NewsApp.Interfaces;
using NewsApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

[assembly: Dependency(typeof(IApiService))]
namespace NewsApp.Services
{
    public class ApiService : IApiService
    {
        private readonly HttpClient _httpClient;

        public ApiService()
        {
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback =
                (message, cert, chain, errors) => { return true; };

            _httpClient = new HttpClient(httpClientHandler);
        }

        public async Task<IEnumerable<News>> GetNewsAsync()
        {
            try
            {
                if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                {
                    var res = await _httpClient.GetAsync("http://christmas96-001-site1.ftempurl.com/api/values");

                    if (res.IsSuccessStatusCode)
                    {
                        var responseContent = res.Content;
                        var result = await responseContent.ReadAsStringAsync();
                        var news =  JsonConvert.DeserializeObject<IEnumerable<News>>(result);
                        return news;
                    }
                }
                else
                    return null;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new List<News>();
            }
            return new List<News>();
        }
    }
}
