﻿using NewsApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsApp.Interfaces
{
    public interface IApiService
    {
        Task<IEnumerable<News>> GetNewsAsync();
    }
}
