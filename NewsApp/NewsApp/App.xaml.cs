﻿using Autofac;
using NewsApp.Interfaces;
using NewsApp.Services;
using NewsApp.ViewModels;
using NewsApp.Views;
using Xamarin.Forms;
using XamarinFormsMvvmAdaptor;

namespace NewsApp
{
    public partial class App : Application
    {
        public static IContainer DiContainer { get; private set; }
        public App()
        {
            InitializeComponent();
            InitContainer();
        }

        void InitContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<MainViewModel>().AsSelf();
            builder.RegisterType<DetailsViewModel>().AsSelf();
            builder.RegisterType<ApiService>().As<IApiService>().SingleInstance();
            builder.RegisterType<NavController>().As<INavController>().SingleInstance();
            DiContainer = builder.Build();
        }

        protected override async void OnStart()
        {
            var navController = DiContainer.Resolve<INavController>();
            await navController.DiInitAsync(DiContainer.Resolve<MainViewModel>());
            MainPage = navController.NavigationRoot;
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
